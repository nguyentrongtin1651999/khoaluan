import 'dart:convert';

import 'dart:typed_data';
import 'package:flutter/material.dart';

import 'package:khoaluan_totnghiep/views/custom_dialog/custom_image.dart';

class FirstPage extends StatefulWidget {
  List<String> images;
  int item_id;
  String org;
  String total_price;
  String customer;
  String others;
  FirstPage(
      {this.images,
      this.item_id,
      this.org,
      this.total_price,
      this.customer,
      this.others});

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  List<String> images;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            width: 300,
            child: CustomImage(
              img: base64toImage(widget.images[0]),
            ),
          ),
          SizedBox(
            width: 24,
          ),
          Container(
              width: 300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 56,
                  ),
                  Text("Customer : ${widget.customer}",
                      style: TextStyle(fontSize: 22, color: Colors.white)),
                  SizedBox(
                    height: 12,
                  ),
                  Text("Total price : ${widget.total_price}",
                      style: TextStyle(fontSize: 22, color: Colors.white)),
                  SizedBox(
                    height: 12,
                  ),
                  Text("Org : ${widget.org}",
                      style: TextStyle(fontSize: 22, color: Colors.white)),
                  SizedBox(
                    height: 12,
                  ),
                  Text("Other : ${widget.others}",
                      style: TextStyle(fontSize: 22, color: Colors.white)),
                  SizedBox(
                    height: 12,
                  ),
                ],
              )),
        ],
      ),
    );
  }

  Image base64toImage(String base64) {
    String tmp = base64.split(',')[1];
    Uint8List bytes = base64Decode(tmp);
    return Image.memory(bytes);
  }
}
