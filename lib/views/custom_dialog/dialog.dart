import 'package:flutter/material.dart';
import 'custom_dialog_box.dart';

class Dialogs extends StatefulWidget {
  @override
  _DialogsState createState() => _DialogsState();
}

class _DialogsState extends State<Dialogs> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        RaisedButton(
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return CustomDialogBox(
                    title: "ADD CLASS",
                    text: "Yes",
                  );
                });
          },
          child: Icon(Icons.add),
        ),
      ],
    ));
  }
}
