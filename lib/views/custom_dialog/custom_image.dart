import 'package:flutter/material.dart';

class CustomImage extends StatelessWidget {
  Image img;
  CustomImage({this.img});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      child: img,
    );
  }
}
