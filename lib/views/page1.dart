import 'dart:async';
import 'dart:html';
import 'dart:convert';
import 'dart:ui';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:khoaluan_totnghiep/widget/glass_card.dart';
import 'custom_dialog/custom_dialog_box.dart';

class PageOne extends StatefulWidget {
  @override
  _PageOneState createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {
  List<String> images;
  var users;
  bool isLoading = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//     this.fetchUser();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.8), BlendMode.darken),
          image: AssetImage(
            "assets/image/img2.jpg",
          ),
        ),
      ),
      child: GlassCardScreen(
        width: MediaQuery.of(context).size.width / 1.5,
        height: MediaQuery.of(context).size.height * 0.9,
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Text(
              "Nhập Ảnh",
              style: TextStyle(color: Colors.purple, fontSize: 26),
            ),
            SizedBox(
              height: 100,
            ),
            RaisedButton(
              onPressed: () async {
                await _setImage();
              },
              child: Icon(
                Icons.add,
                size: 150,
                color: Colors.purple,
              ),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
            ),
          ],
        ),
      ),
    );
  }

  // lấy dữ liệu

// chuyển ảnh
  Image base64toImage(String base64) {
    String tmp = base64.split(',')[1];
    Uint8List bytes = base64Decode(tmp);
    return Image.memory(bytes);
  }

  // lấy ảnh trong thư mục
  Future<void> _setImage() async {
    final completer = Completer<List<String>>();
    InputElement uploadInput = FileUploadInputElement();
    uploadInput.multiple = true;
    uploadInput.accept = 'image/*';
    uploadInput.click();

    uploadInput.addEventListener('change', (e) async {
      final files = uploadInput.files;
      Iterable<Future<String>> resultsFutures = files.map((file) {
        final reader = FileReader();
        reader.readAsDataUrl(file);
        reader.onError.listen((error) => completer.completeError(error));
        return reader.onLoad.first.then((_) => reader.result as String);
      });

      final results = await Future.wait(resultsFutures);
      completer.complete(results);
    });
    document.body.append(uploadInput);
    final List<String> _images = await completer.future;
    setState(() {
      images = _images;
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomDialogBox(
              title: "ADD CLASS",
              text: "Yes",
              img: this.images,
            );
          });
      // lấy dữ liệu
      // this.fetchUser();
    });
    uploadInput.remove();
  }
}
