import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PageTwo extends StatefulWidget {
  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  var users;
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.fetchUser();
  }

  @override
  Widget build(BuildContext context) {
    if (users == null || isLoading) {
      return Center(
          child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
      ));
    }
    return Text(
      users['data']['customer'],
      style: TextStyle(fontSize: 20, color: Colors.black),
    );
  }

  fetchUser() async {
    setState(() {
      isLoading = true;
    });
    var url = "http://127.0.0.1:8000/items/4";
    var response = await http.get(url);
    print(response.body);
    if (response.statusCode == 200) {
      var items = json.decode(response.body);
      setState(() {
        users = items;
        isLoading = false;
      });
    } else {
      users = {};
      isLoading = false;
    }
  }
}
