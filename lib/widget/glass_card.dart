import 'dart:ui';

import 'package:flutter/material.dart';

class GlassCardScreen extends StatelessWidget {
  double height;
  double width;
  Widget child;
  GlassCardScreen({this.height, this.width, this.child});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: [
          Positioned(
            top: 50,
            left: width / 4,
            width: width,
            height: height,
            // Note: without ClipRect, the blur region will be expanded to full
            // size of the Image instead of custom size
            child: ClipRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Container(
                    height: height,
                    width: width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Colors.white.withOpacity(0.2),
                              Colors.white.withOpacity(0.05)
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight),
                        border:
                            Border.all(color: Colors.white.withOpacity(0.08)),
                        borderRadius: BorderRadius.circular(10)),
                    child: child),
              ),
            ),
          )
        ],
      ),
    );
  }
}
